function poscode
% io : Item vs Order
[homedir, FileName] = fileparts(mfilename('fullpath'));
cd(homedir);
close all

% 5-Add/Lag/Lag-Y_0-7.png
%Models = {'Add' 'Norm'};
Models = {'Add'};
Iz = 5; % 5-item seq, 3-item seq

pi_0        = 1; % initial \pi
Betas       = 0.8; %0:0.1:1.2;
NumBetas    = size(Betas,2);
NamesItem   = {'A' 'B' 'C' 'D' 'E'};
NamesPos    = {'1' '2' '3' '4' '5'};
NumVoxels   = 20; % number of voxels / neurons / response units
Voxels      = [1 18]; % two random units out of 20
NumSamples  = 1; % 1 if you want to run it once, 10^3 if distribution of stats

for i = 1:size(Iz,2)
    NumItems = Iz(i);     % number of items in an individual sequence
    for m = 1:size(Models,2)
        
        Model = Models{m};
        
        PlotDir     = ['/home/kk02/Docs/_pubs/07_io/fig/' num2str(NumItems) '-' Model] ;
        %PlotDir     = [homedir '/fig/' num2str(NumItems) '-' Model] ;
        PlotFormat  = '-pdf';
        PlotAxes    = [0 1.7 0 1.7];
        Pos         = 1:NumItems;             % position labels
        NumSeqs     = NumItems * 2;
        Seqs        = latin_square(NumItems);
        Seqs        = repmat(Seqs, 2, 1);
        Seqs        = Seqs(1:NumSeqs,:);
        LabelItem   = reshape(Seqs', 1, []);
        LabelPos    = kron(ones(1,NumSeqs), Pos);
        DM          = Dmat(LabelPos);
        LagValues   = unique(DM.LAGr(DM.I));
        
        
        if strfind(Model, 'Norm')
            Betas       = 0:0.1/2:1.2/2;
        end
        
        for b = 1:NumBetas
            
            Beta = Betas(b);
            
            BetaStr = ['_' strrep(num2str(Beta), '.', '-')];
            
            for j = 1:NumSamples
                
                % uncorrelated patterns representing each items
                if NumSamples > 1
                    Items    = rand(NumVoxels, 10); % Items
                    NoiseVar = 1/15; % Noise
                    Noise    = random('Normal', 0, NoiseVar, NumVoxels, 50);
                    save('Items.mat', 'Items');
                    save('Noise.mat', 'Noise');
                else
                    load('bak/Items.mat', 'Items');
                    load('bak/Noise.mat', 'Noise');
                end
                
                Items = Items(:,1:NumItems);
                Noise = Noise(:,1:NumSeqs*NumItems);
                
                % response matrix of sequences
                R = Items(:, LabelItem);
                
                % add noise
                Rn = R + Noise;
                
                %% overlaying model
                Y = nan(NumVoxels, NumItems, NumSeqs); % empty voxels' response matrix
                % fill out voxels' response matrix P
                for s = 1:NumSeqs
                    I = [];
                    for p = Pos
                        
                        I  = Items(:, Seqs(s,p));
                        if p == 1
                            Y(:,p,s) = I;
                        else
                            switch Model
                                case {'Add' 'AddZ'}
                                    Y(:,p,s) = I + (Y(:,p-1,s) * Beta);
                                case {'Norm' 'NormV'}
                                    Y(:,p,s) = (I * (1-Beta)) + (Y(:,p-1,s) * Beta);
                            end
                        end
                        
                        % z-scoring
                        if strfind(Model, 'Z')
                            Y(:,p,s) = zscore(Y(:,p,s));
                        end
                        
                    end % positions within seq
                end % sequences
                
                % unpack P from 3d to 2d
                Y = reshape(Y, NumVoxels, []);
                % add noise
                Y = Y + Noise;
                
                %% lda
                [Wi, Ami(j,b)] = plda(Y(Voxels,:)', LabelItem');
                [Wp, Amp(j,b)] = plda(Y(Voxels,:)', LabelPos');
                
                %% lag effect
                B = corr(Y(:,DM.ReOrder));
                for g = 1:size(LagValues,1)
                    LagMeans(g) = mean(B(DM.LAGr == LagValues(g)));
                end
                LagCoeff = [ones(NumItems,1) Pos'] \ LagMeans';
                LagSlope(j,b) = LagCoeff(2,1);
            end % numsamples
            
            if NumSamples > 1
                %Bins = 0:0.1:1;
                %PlotHist(Ami, Bins);
                %Bins = linspace(-0.1,0.1,40);
                %PlotHist(LagSlope(:,2), Bins);
            end
            
            if NumSamples == 1
                
                % means plots
                do_means = 0;
                if do_means
                    switch Model
                        case 'AddZ'
                            POpts.PlotAxes = [-2 2 -0.5 1.65];
                        otherwise
                            POpts.PlotAxes = [-0.1 1.75 -0.1 1.75]; % Add, MV
                    end
                    POpts.Figure = 1;
                    PlotMeans({Rn,Y}, LabelItem, Voxels, POpts);
                    export_fig(fullfile(PlotDir, 'Means', ['Means-Item' BetaStr]), PlotFormat);
                    PlotMeans({Rn,Y}, LabelPos, Voxels, POpts);
                    export_fig(fullfile(PlotDir, 'Means', ['Means-Pos' BetaStr]), PlotFormat);
                end
                
                % similarity plots
                do_sim_M = 1;
                if do_sim_M
                    LagPlot(Y, LabelPos, DM)
                    export_fig(fullfile(PlotDir, 'Lag', ['Lag-Y' BetaStr]), '-pdf');
                end
            end % NumSamples == 1
            
        end % betas
        
        if NumBetas > 1 && NumSamples > 1
            
            % item and position decoding
            figure('Position', [100 100 400, 300])
            do_ebar(Ami, Betas)
            hold on
            Opts.Color   = [1 0 0];
            Opts.HorLine = 1/NumItems;
            Opts.XLabel  = 'Beta';
            Opts.YLabel  = 'Classification accuray';
            Opts.YLim    = [0 1.1];
            do_ebar(Amp, Betas, Opts)
            export_fig(fullfile(PlotDir, 'LDA'), PlotFormat);
            hold off
            
            % lag effect size
            figure('Position', [100 100 400, 300])
            LOpts.XLabel   = 'Beta';
            LOpts.YLabel   = 'Lag slope';
            LOpts.YLim     = [-0.05 0.05];
            LOpts.HorLine  = 0;
            do_ebar(LagSlope, Betas, LOpts);
            export_fig(fullfile(PlotDir, 'Lag'), PlotFormat);
        end
        
    end % models
end % items

%% plots
% item pattern plots
do_R_plots = 0;
if do_R_plots
    % single items
    PlotResponse(Items, NamesItem, NamesPos, NumItems);
    export_fig(fullfile(PlotDir, 'R-Items'), PlotFormat);
    
    % sequences
    NamesItem = NamesItem(:, LabelItem);
    NamesPos  = repmat(NamesPos(1:NumItems), 1, NumSeqs);
    PlotResponse(R, NamesItem, NamesPos, NumItems);
    export_fig(fullfile(PlotDir, 'R-Seqs'), PlotFormat);
    
    % Rn = R + Noise
    PlotResponse(Rn, NamesItem, NamesPos, NumItems);
    export_fig(fullfile(PlotDir, 'R-Seqs-Noise'), PlotFormat);
end

% LDA of item patterns
do_R_lda = 0;
if do_R_lda
    PlotLda(LabelItem, NamesItem, Rn, Voxels);
    export_fig(fullfile(PlotDir, 'LDA-Rn-Item'), PlotFormat);
    PlotLda(LabelPos, NamesPos, Rn, Voxels);
    export_fig(fullfile(PlotDir, 'LDA-Rn-Pos'), PlotFormat);
end

% plot lda
do_lda_Y = 0;
if do_lda_Y
    PlotLda(LabelItem, NamesItem, Y, Voxels);
    export_fig(fullfile(PlotDir, 'LDA-RnM-Item'), PlotFormat);
    PlotLda(LabelPos, NamesPos, Y, Voxels);
    export_fig(fullfile(PlotDir, 'LDA-RnM-Pos'), PlotFormat);
end

end

function PlotHist(A, Bins)
H = histc(A, Bins);
BOpts.XLim = [min(Bins) max(Bins)];
figure('Position', [50, 50, 450, 350])
do_bar(H, [], Bins, BOpts)
XTick = linspace(min(Bins),max(Bins),5);
set(gca,'XTick',XTick,'XTickLabel', XTick);
%xlabel('LDA accuracy')
end

function PlotResponse(R, NamesItem, NamesPos, NumItems)
ColWidth = 18;
figure('Position', [100, 300, ColWidth*size(R,2) 200]);
imagesc(R);

% add column labels
set(gca, ...
    'XTick', 1:size(R,2), 'XTickLabel', NamesItem, 'XAxisLocation','top');

axes('Position', get(gca,'Position'),...
    'XAxisLocation','bottom', 'XLim', get(gca, 'XLim'), 'XTick', get(gca, 'XTick'), ...
    'XTickLabel', NamesPos, ...
    'YAxisLocation','right', 'YTick', [], ...
    'Color','none');

% add lines to mark sequences
if size(R,2) > NumItems
    NumLines = (size(R,2) / NumItems) - 1;
    Lines = [NumItems:NumItems:NumLines*NumItems] + 0.5;
    ylim = get(gca, 'YLim');
    for l = 1:size(Lines,2)
        line([Lines(l) Lines(l)],[0 1], 'Color', 'k', 'LineStyle', '-', 'LineWidth', 1);
    end
end

% make plot background white
set(gcf, 'color', 'w');

end

function PlotLda(Labels, Names, Y, Voxels)
figure('Position', [100, 300, 250 250]);
if nargin < 4
    LOpts.Plot = 0;
    Voxels = Lda(Labels, Y, LOpts);
end
LOpts.Plot   = 1;
LOpts.Voxels = Voxels;
LOpts.MarkerSize = 4;
%LOpts.PlotAxes = [0 1 0 1];
%LOpts.PlotAxes = [-0.2 1.2 -0.2 1.2];
%LOpts.PlotAxes = [-0.1 1 0.2 1.4];
LOpts.PlotAxes = [-0.1 1.75 -0.1 1.75]; % Add, MV
%LOpts.PlotAxes = [0 1.7 0 1.7];
%LOpts.PlotAxes = [-1.5 1 0 1.6];
LOpts.LegendLocation = 'northeast';

Num = size(unique(Labels),2);

LOpts.Legend = Names(1:Num);
Lda(Labels,  Y, LOpts);
xlabel(['Voxel ' num2str(Voxels(1))]), ylabel(['Voxel ' num2str(Voxels(2))]);

% make plot background white
set(gcf, 'color', 'w');

end

function Seqs = latin_square(n)
% create all equally unique
P = perms(1:n);
P = sortrows(P);
% take 1st perms as the seed
Seqs = P(1,:);
do = 1;

while do
    H = pdist2(Seqs, P, 'hamming');
    ix = find(sum(H,1) == do);
    if ~isempty(ix)
        AddSeq = P(ix(1),:);
        Seqs = [Seqs; AddSeq];
        do = do + 1;
    else
        do = 0;
    end
end

end

function D = Dmat(X)
% X can be either logical deisgn matrix where
%  rows = num of classes
%  cols = binalry class membership
% e.g
% X = [1 0 0; 0 1 0; 0 0 1];
% alternatively X can be column vector of labels
% L = [1 2 3];

%% test if X is logical
if islogical(X)
    % condition labels
    [d, L] = max(X, [], 1);
else
    %warning('X is not logical!')
    disp('X is not logical!');
    L = X;
    X = ind2log(L)';
end

%% create matrices
[Lr, ReOrder] = sort(L);
% lower triangle of full X
I = logical(tril(ones(size(X,2)),-1));
% similarity DM
G = xRSA(X);
% unique similarities
GS = unique(G(I));
% lag

% reordered
Xr  = X(:, ReOrder);
[Gr, Dr, LAGr]  = xRSA(Xr);
GSr = unique(Gr(I));


%% arrange all matrices to a single struct
%D = struct([]);
w = warning ('off','all');

SubFields = {'L' 'Lr' 'X' 'Xr' 'ReOrder' 'I' 'G' 'Gr' 'GS' 'GSr' 'LAGr'};
for f = 1 : size(SubFields,2)
    D.(SubFields{f}) = eval(SubFields{f});
end

w = warning ('on','all');

end

function T = ind2log(I)
w = max(I);
l = length(I);
for i=1:l
    T(i,1:w) = zeros(1,w);
    if I(i)>0
        T(i,I(i)) = 1;
    end
end
T = logical(T);
end

function [G, D, Lag] = xRSA(X, XS)

% X has to be a binary matrix
% cols = detatapoints
% rows = conditions
assert(islogical(X), 'X is not a logical matrix!');

% make sure that there are more datapoints than classes
assert(size(X,1) < size(X,2), 'X is not a valid design matrix!');

if nargin < 2
    XS = true(1, size(X,2));
end

[d.Lr, L] = max(X, [], 1); % condition labels
XSize = size(X,2);
% % RSA & Corr
% rsa index for predictive coding
[d.u1, d.u2, Pred] = unique([XS; L]', 'rows');

G = Pred(:,ones(1,size(Pred,1)));
G = [reshape(G,[],1) reshape(G',[],1)];
[d.gu1, d.gu2, G] = unique(G,'rows');
G = reshape(G, XSize, XSize);
%imagesc(G);

% number of conditions
n = size(X,1);
% cell labels
CellLabels = kron(ones(1,n), 1:n) + kron((1:n)*10, ones(1,n));
% matrix with cell labels
G = CellLabels(G);
%imagesc(G);

% make lower and upper triangle labels the same
SameLabelsDiag = 1;
if SameLabelsDiag
    Gu = triu(G,1);
    Gf = rot90(flipud(Gu),-1);
    LowerIndex = logical(tril(G,-1));
    G(LowerIndex) = Gf(LowerIndex);
end
% lower triangle
G = tril(G,-1);
%U = logical(tril(ones(size(X,2)),-1));

% contruct a diagonal analysis matrix
% for analyses like:
% within-condition similarity > between condition-similarity
D = arrayfun(@ones, nozeros(hist(L)), 'UniformOutput', false);
D = blkdiag(D{:});
D(D==0) = 2; % so that 1=within cond, 2=btw cond, 0=ignore
D = tril(D,-1);

% create a lag matrix
%Lag = pdist2([1:n]', [1:n]');
Lag = squareform(pdist([1:n]'));
NumSameCond = unique(nozeros(hist(L)));
Lag = arrayfun(@(x) ones(NumSameCond).*x, Lag, 'UniformOutput', false);
Lag = cell2mat(Lag);
Lag = Lag + 1;
Lag = tril(Lag,-1);

end

function N = nozeros(M)
% NOZEROS  returns nonzero items from matrix/vector
% NOZEROS (M)
%     Example:
%           A = [0 1 0 1 0];
%           A = nozeros(A)
%   A =
%     1     1
...
    if nargin < 1, disp('No input specified. '); return ; end
...
    
N = M(M~=0);
end