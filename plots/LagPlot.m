function V = LagPlot(Y, X, DM)

figure('Units', 'pixels', 'Position', [0 300 500 200]);
subplot = @(m,n,p) subtightplot (m, n, p, [0.25 0.1], [0.12 0.12], [0.05 0.05]);

X = DM.X;
Pos    = 1:size(X,1);
Self   = find(eye(max(Pos))); % index of the diagonal of the similarity matrix
NumPos = max(Pos);
Nine   = find(ones(NumPos));

%% Get subject SVM data
Yu = Y(:, DM.ReOrder);
% z-score
Y = zscore(Y);
Yr = Y(:, DM.ReOrder);
B = corr(Yr);

% average similarity values
for g = 1:size(DM.GSr,1)
    Means(g) = mean(B(DM.Gr == DM.GSr(g)));
    Std(g)   = std(B(DM.Gr == DM.GSr(g)));
    LM(g)    = loftusmason(B(DM.Gr == DM.GSr(g)));
end

% lag similarity values
LagValues = unique(DM.LAGr(DM.I));
for g = 1:size(LagValues,1)
    LagMeans(g) = mean(B(DM.LAGr == LagValues(g)));
    LagStd(g)   = std(B(DM.LAGr == LagValues(g))) ./ (max(Pos)-1);
    LagLM(g)    = loftusmason(B(DM.LAGr == LagValues(g)));
end
LagSlope = [ones(NumPos,1) Pos'] \ LagMeans';

assert(unique(sum(X)) == 1, 'This doesnt look like SVM design!');

Sim.B       = B;
Sim.MeansSq = fillmat(Means, max(Pos)); % in full matrix form
Sim.StdSq   = fillmat(Std, max(Pos)); % in full matrix form


SimAvg   = Sim.MeansSq(:)';
SimAvgE  = Sim.StdSq(:);
SimSlope = diag(Sim.MeansSq);
SimSlope = [ones(NumPos,1) Pos'] \ SimSlope;
SlopeP   = 0;
%

subplot(1,2,1)

ylim = [0 .7];
simplot(reshape(SimAvg, NumPos, NumPos), Pos, Pos, ylim);
cb = colorbar;
set(cb, 'FontSize', 10)
%set(cb, 'Ticks', ylim(1):0.1:ylim(2), 'AxisLocation','in');
title(['Pattern similarity'], 'FontSize', 10);

subplot(1,2,2)
BOpts.Cl = [];
BOpts.Slope = LagSlope(1) + Pos .* LagSlope(2);
BOpts.FontSize = 10;
Ymax = max(LagMeans + LagStd);
Ymin = min(LagMeans - LagStd);
ylb = max(LagStd)/2;
BOpts.YLim  = [Ymin-ylb Ymax+ylb];
BOpts.L = {'Same' '1' '2' '3' '4' };
do_bar(LagMeans, LagStd, Pos, BOpts);
set(gca, 'YTick', [round(mean(LagMeans),2) round(Ymax,2)]);
title(['Lag similarity'], 'FontSize', 10);

set(gcf, 'color', 'w');


end % main function

function simplot(B, L, XLabels, ylim)

% labels
if nargin < 2, L = []; end
% Xlabels
if nargin < 3, XLabels = []; end
% ylim
if nargin < 4, ylim = [min(B(:)) max(B(:))]; end

hi = imagesc(B, ylim); hold on

% put boundary lines on plot
[d.u1, Bo] = unique(L);
Bo = Bo + 0.5;
y2 = size(L,2);

% if more than one cells per class label
if size(L,2) > size(XLabels,2)
    % line([x1 x2],[y1 y2])
    for bb = 1:size(Bo,2)-1
        line([Bo(bb) Bo(bb)], [0 y2+1], 'Color', 'k', 'LineWidth', 1);
        line([0 y2+1], [Bo(bb) Bo(bb)], 'Color', 'k', 'LineWidth', 1);
    end
    Bo_margin = ceil(Bo(2)/2);
    XTick = Bo + Bo_margin - 1;
else
    XTick = 1:size(L,2);
end

set(gca, 'XTick', XTick, 'YTick', XTick, ...
    'FontSize', 10, ...
    'XAxisLocation', 'bottom');

if iscell(XLabels) && isnumeric(L) % for LabelsPred
    set(gca, 'XTickLabel', XLabels(4:6), 'YTickLabel', XLabels(1:3));
else
    set(gca, 'XTickLabel', XLabels, 'YTickLabel', XLabels);
end
end

function M = fillmat(m, d)
M = zeros(d);
u1 = logical(triu(ones(d)));
u2 = logical(triu(ones(d),1));
l2 = logical(tril(ones(d),-1));
M(u1) = m;
offDiag = squareform(M(u2));
M(l2) = offDiag(l2);
end