function PlotMeans(Y, Labels, Voxels, Opts)
% Y         is a cell array of 2D matrices, where each matrix has labels as columns
% and voxels as rows
% Labels    is a cell array of matrix labels; has to be the same size as Y
% Voxels    (optional) specifies which rows to use from Y

% check if Y and Labels are cell arrays
assert(iscell(Y) && isnumeric(Labels), 'Y must be cell & Labels double')

if nargin < 4, Opts = []; end
if ~isfield(Opts, 'Figure'),        Opts.Figure = 0;        end; % OWN FIGURE HANDLER
if ~isfield(Opts, 'PlotAxes'),      Opts.PlotAxes = [];     end; % predefined plot axes
if ~isfield(Opts, 'DoVarEllipse'),  Opts.DoVarEllipse = 0;  end; % draw variance ellipses
if ~isfield(Opts, 'DataMarkers'),   Opts.DataMarkers = 1;   end; % draw variance ellipses
if ~isfield(Opts, 'FontSize'),      Opts.FontSize = 10;   end; % draw variance ellipses
if ~isfield(Opts, 'Names'), % cond labels
    Names = arrayfun(@(x) {num2str(x)}, unique(Labels)); 
else
    Names = Opts.Names;
end;

Cols   = {'k', 'r', 'b', 'g' 'y'};
Shapes = {'o', 'o', 'o', 'o' 'o'};

Num = size(unique(Labels),2); % number of conds

%% do it

Yb = Y{1}(Voxels,:);
Ya = Y{2}(Voxels,:);

for v = 1:size(Voxels,2)
    for p = 1:Num
        m_b(v,p) = mean(Yb(v,Labels == p));
        s_b(v,p) = std( Yb(v,Labels == p));
        m_a(v,p) = mean(Ya(v,Labels == p));
        s_a(v,p) = std( Ya(v,Labels == p));
    end
end

m = [m_b m_a];
S = [s_b s_a];


if Opts.Figure
    figure('Position', [100, 300, 250 250]);
    hold on
end

MeanMarkerSize = 6;
DataMarkerSize = 3;

%% start plotting
hold on

%plot individual data
if Opts.DataMarkers
    Sya = gscatter(Ya(1,:), Ya(2,:), Labels, cell2mat(Cols(1:Num)),...
        cell2mat(Shapes(1:Num)), DataMarkerSize, 'off'); 
    Syb = gscatter(Yb(1,:), Yb(2,:), Labels, cell2mat(Cols(1:Num)),...
        cell2mat(Shapes(1:Num)), DataMarkerSize, 'off');  
end

% plot means
Sb = gscatter(m_b(1,:), m_b(2,:), 1:Num, cell2mat(Cols(1:Num)),...
    cell2mat(Shapes(1:Num)), MeanMarkerSize, 'off');
Sa = gscatter(m_a(1,:), m_a(2,:), 1:Num, cell2mat(Cols(1:Num)),...
    cell2mat(Shapes(1:Num)), MeanMarkerSize, 'off');

for c = 1:Num    
    idx = ( Labels == c );
    % add transform vector
    line([m_b(1,c) m_a(1,c)], [m_b(2,c) m_a(2,c)], 'Color', Cols{c}, 'LineStyle', '-', 'LineWidth', 1);
    % variance ellipses
    if Opts.DoVarEllipse
        LineYb = VarEllipse(Yb, idx);
        set(LineYb, 'LineStyle', ':', 'Color', Cols{c})
        LineYa = VarEllipse(Ya, idx);
        set(LineYa, 'LineStyle', '-', 'Color', Cols{c})
    end
    % marker colours
    Sa(c).MarkerFaceColor = Cols{c};
    Sb(c).MarkerFaceColor = 'w';
    Sya(c).MarkerFaceColor = Cols{c};      
end

if ~isempty(Opts.PlotAxes)
    axis(Opts.PlotAxes);   
end

% legend
legend(Names(1:Num), 'Location', 'best');

xlabel(['Voxel ' num2str(Voxels(1))]), ylabel(['Voxel ' num2str(Voxels(2))]);

set(gca, 'FontSize', Opts.FontSize)

% make plot background white
set(gcf, 'color', 'w');

hold off

end

function Line = VarEllipse(X, idx)

X = X';

% substract mean
Mu = mean( X(idx,:) );
X0 = bsxfun(@minus, X(idx,:), Mu);

STD = 1;                     % 2 standard deviations
conf = 2*normcdf(STD)-1;     % covers around 95pc of population
scale = chi2inv(conf,2);     % inverse chi-squared with dof=dimensions

Cov = cov(X0) * scale;
[V, D] = eig(Cov);

% eigen decomposition [sorted by eigen values]
%[V D] = eig( X0'*X0 ./ (sum(idx)-1) );     % cov(X0)
[D, order] = sort(diag(D), 'descend');
D = diag(D);
V = V(:, order);

t = linspace(0,2*pi,100);
e = [cos(t) ; sin(t)];        % unit circle
VV = V*sqrt(D);               % scale eigenvectors
e = bsxfun(@plus, VV*e, Mu'); %' project circle back to orig space

% plot cov and major/minor axes
Line = plot(e(1,:), e(2,:), 'Color','k');
%quiver(Mu(1),Mu(2), VV(1,1),VV(2,1), 'Color','k')
%quiver(Mu(1),Mu(2), VV(1,2),VV(2,2), 'Color','k')
end