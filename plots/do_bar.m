function do_bar(Y, sme, X, Opts)

if nargin < 1, error('No input') ; end
if nargin < 2, sme  = []; end
if nargin < 3, X    = 1:size(Y,2); end
if nargin < 4, Opts = []; end

if ~isfield(Opts, 'L'),         Opts.L          = [];  end;
if ~isfield(Opts, 'Cl'),        Opts.Cl         = []; end;
if ~isfield(Opts, 'PlotTitle'), Opts.PlotTitle  = []; end;
if ~isfield(Opts, 'YLim'),      Opts.YLim       = []; end;
if ~isfield(Opts, 'XLim'),      Opts.XLim       = []; end;
if ~isfield(Opts, 'FontSize'),  Opts.FontSize   = 8; end;
if ~isfield(Opts, 'LineWidth'), Opts.LineWidth  = 1; end;
if ~isfield(Opts, 'Slope'),     Opts.Slope      = []; end;
if ~isfield(Opts, 'SlopeCol'),  Opts.SlopeCol   = 'red1'; end;



% plot params
c = plot_colours;
 
% for 3x3 designs
if size(X,1) == 9
    X = [1:3 5:7 9:11];
    Opts.XLim = [0 max(X)+1];
    i = [1 5 9];
    j = [2:4 6:8];    
    b1 = bar(X(i),Y(i)); hold on;
    b2 = bar(X(j),Y(j)); hold on;
    b3 = bar([4 8],[0 0]); hold on;
    
    set(b1, 'BarWidth', 0.17, 'FaceColor', c.gre3, 'EdgeColor', 'none');
    set(b2, 'FaceColor', c.grey, 'EdgeColor', 'none');
    set(b3, 'BarWidth', 0, 'FaceColor', 'none', 'EdgeColor', 'none');
else % normal bar
    hBar = bar(X,Y); hold on;
    set (hBar, ...
        'FaceColor',  c.grey, ...
        'EdgeColor',  c.grey);
end

% Errorbars
if ~isempty(sme)
    hErr = errorbar(X, Y, sme, 'xr'); hold on;
    set(hErr                           , ...
        'Marker'          , '.'        , ...
        'MarkerSize'      , 6          , ...
        'LineWidth'       , Opts.LineWidth  , ...
        'MarkerFaceColor' , c.blac     , ...
        'MarkerEdgeColor' , c.blac      , ...
        'Color'           , c.blac     );
end

% slope line
if ~isempty(Opts.Slope)
    plot(X, Opts.Slope, 'LineStyle', '-', 'Color', c.(Opts.SlopeCol), ...
        'LineWidth', Opts.LineWidth);
end

% chancelevel line
if ~isempty(Opts.Cl)
    line([min(X)-1 max(X)+1], [Opts.Cl Opts.Cl], 'Color', c.red1, 'LineWidth', Opts.LineWidth);
end

% title
if ~isempty(Opts.PlotTitle)
    title(Opts.PlotTitle, 'FontSize', Opts.FontSize+2);
end

if ~isempty(Opts.L)
    set(gca, ...
        'FontSize',         Opts.FontSize, ...
        'XAxisLocation',    'bottom', ...
        'XTick',            X, ...
        'XTickLabel',       Opts.L);
end

if ~isempty(Opts.YLim)
    set(gca, 'YLim', Opts.YLim);
end
if ~isempty(Opts.XLim)
    set(gca, 'XLim', Opts.XLim);
end

% this row for some reason prevents resizing of the pdf
if nargin > 3
    %set(gca, 'YTick', []);
end
end
