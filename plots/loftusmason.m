function [sme, E] = loftusmason(Y)
%LOFTUSMASON Summary of this function goes here
%   Detailed explanation goes here

% Loftus & Masson (1994) STE
E.subj_mean = nanmean(Y,2);
E.sm = mean(E.subj_mean);
E.subj_amean = E.subj_mean - E.sm;
E.Yd = Y-kron(ones(1,size(Y,2)),E.subj_amean);
E.Y = nanmean(E.Yd,1);
% nan hack for std
for ei=1:size(E.Yd,2)
    E.E(ei) = std(nonans(E.Yd(:,ei)),1);
end
sme = E.E;
end

