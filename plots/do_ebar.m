function SA = do_ebar(Y, X, Opts)
% design matrix
if nargin < 2, X = 1:size(Y,2); end
% Opts
if nargin < 3, Opts = []; end
if ~isfield(Opts, 'Slope'),     Opts.Slope      = 0; end;
if ~isfield(Opts, 'VarType'),   Opts.VarType    = 'std'; end;
if ~isfield(Opts, 'YLim'),      Opts.YLim       = []; end;
if ~isfield(Opts, 'XLim'),      Opts.XLim       = []; end;
if ~isfield(Opts, 'HorLine'),   Opts.HorLine    = []; end;
if ~isfield(Opts, 'Title'),     Opts.Title      = []; end;
if ~isfield(Opts, 'XLabel'),    Opts.XLabel     = []; end;
if ~isfield(Opts, 'XTick'),     Opts.XTick      = []; end;
if ~isfield(Opts, 'XLabel'),    Opts.XLabel     = []; end;
if ~isfield(Opts, 'YLabel'),    Opts.YLabel     = []; end;
if ~isfield(Opts, 'LineColor'), Opts.LineColor  = []; end;
if ~isfield(Opts, 'LineStyle'), Opts.LineStyle  = 'none'; end;
if ~isfield(Opts, 'FontSize'),  Opts.FontSize   = []; end;
if ~isfield(Opts, 'Color'),     Opts.Color      = [0 0 0]; end;
if ~isfield(Opts, 'E'),         Opts.E          = []; end;
if ~isfield(Opts, 'DoBar'),     Opts.DoBar      = 0; end;

% if errorbar values already given
if ~isempty(Opts.E)
    E = Opts.E;
else
    % get ebar values from data
    switch Opts.VarType
        case 'std'
            denom = 1;
            E = std(Y);
        case 'sme'
            denom = sqrt(length(Y)-1);
            E = std(Y)/denom;
        case 'lm'
            E = loftusmason(Y);
        otherwise
            error('Unknown errorbar definition')
    end
end

Y = nanmean(Y,1);
X = X(1,:);

if Opts.DoBar
    c = plot_colours;
    hBar = bar(X,Y); hold on;
    set (hBar, ...
        'FaceColor',  c.grey, ...
        'EdgeColor',  c.grey);
end

% graphics
hEline = errorbar(X, Y, E);
hold on;


Pos = get(gcf, 'Position');
% depending on plot size Line borderr mand marker sizes
if Pos(3) > 500 % width of the plot
    LineWidth  = 1;
    MarkerSize = 4;
else
    LineWidth  = 1;
    MarkerSize = 6;
end

set(hEline                              , ...
    'LineStyle'       , Opts.LineStyle  , ...
    'LineWidth'       , LineWidth   ,...
    'Marker'          , 'o'         , ...
    'MarkerSize'      , MarkerSize  , ...
    'MarkerFaceColor' , Opts.Color , ...
    'MarkerEdgeColor' , Opts.Color , ...
    'Color'           , Opts.Color    );

%% slope
switch Opts.Slope
    case {1,2,3}
        for si=1:e_slope
            
            clear BX BY
            
            switch e_slope
                case 1
                    BX = X;
                    BY = Y;
                case 2
                    if (si==1)
                        bi = [(min(X)):max(X)/2];
                        BX = X(:,bi);
                        BY = Y(:,bi);
                    elseif (si==2)
                        bi = [(max(X)/2)+1:max(X)];
                        BX = X(:,bi);
                        BY = Y(:,bi);
                    end
                case 3
                    if (si==1)
                        bi = [(min(X)):max(X)/2];
                        BX = X(:,bi);
                        BY = Y(:,bi);
                    elseif (si==2)
                        bi = [(max(X)/2)+1:max(X)];
                        BX = X(:,bi);
                        BY = Y(:,bi);
                    elseif (si==3)
                        BX = X;
                        BY = Y;
                    end
            end
            
            [SA{si}.Beta, SA{si}.Dev, SA{si}.Stats] = glmfit(BX,BY,'normal');
            SA{si}.Slope = SA{si}.Beta(2); % slope
            SA{si}.Inter = SA{si}.Stats.beta(1); % intercept or alpha
            SA{si}.T     = SA{si}.Stats.t(2); % t-value
            B = glmval(SA{si}.Beta, BX, 'identity');
            
            
            hFit{si}  = line(BX, B);    % fitted line
            
            set(hFit{si}                                , ...
                'LineStyle'       , ':'            , ...
                'LineWidth'       , 1             ,...
                'Marker'          , 's'             , ...
                'MarkerSize'      , 1               , ...
                'MarkerFaceColor' , [1 1 1]     , ...
                'Color'           , 'k'    );
            ptext = sprintf('%0.3f',SA{si}.Stats.p(2));
            ttext = sprintf('%0.3f',SA{si}.Stats.t(2));
            Textbox = [' - Slope p=',ptext];
            
            %             ty = Y(1)*0.95;
            %             switch si
            %                 case 3
            %                     text(BX(2), ty, Textbox{si}) % x value, y value, textbox
            %                 otherwise
            %                     text(BX(2), ty, Textbox{si}) % x value, y value, textbox
            %                     text(BX(6), ty, Textbox{si+1}) % t
            %             end
            
        end
end

%% horisontal line: chance level or mean etc
if ~isempty(Opts.HorLine)if ~isempty(Opts.YLim)
    set(gca, 'YLim', Opts.YLim);
end
    horz_vals = kron(ones(size(Y,2),1), Opts.HorLine);
    cLine = plot(X, horz_vals);
    set(cLine, 'LineWidth', LineWidth, 'Color', 'r', 'LineStyle', ':');
end

%% plot pr0perties

set(gca, ...
    'Box'         , 'off'     , ...
    'TickDir'     , 'out'     , ...
    'TickLength'  , [.02 .02] , ...
    'XMinorTick'  , 'off'      , ...
    'YMinorTick'  , 'off'      , ...
    'XColor'      , [0 0 0]   , ...
    'YColor'      , [0 0 0]   , ...    
    'FontName'    , 'Helvetica');
%XLim
Xspan = abs(max(X) - min(X));
Buffer = 0.1*Xspan;

if size(Y,2) > 1
    if X(1) == 0
        set(gca, 'XLim', [-Buffer max(X)+Buffer]);
    else
        set(gca, 'XLim', [min(X)-Buffer max(X)+Buffer]);
    end
end

if ~isempty(Opts.XTick)
    set(gca, 'XTickLabel', Opts.XTick);
end
if ~isempty(Opts.YLim)
    set(gca, 'YLim', Opts.YLim);
end
if ~isempty(Opts.XLim)
    set(gca, 'XLim', Opts.XLim);
end
if ~isempty(Opts.XLabel)    
    xlabel(Opts.XLabel);
end
if ~isempty(Opts.YLabel)    
    ylabel(Opts.YLabel);
end
if ~isempty(Opts.FontSize)
    set(gca, 'FontSize', Opts.FontSize);    
end

set(gcf, 'color', 'w');


