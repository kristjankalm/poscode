function simplot(B, L, XLabels, ylim)
    
    % labels
    if nargin < 2, L = []; end
    % Xlabels
    if nargin < 3, XLabels = []; end
    % ylim
    if nargin < 4, ylim = [min(B(:)) max(B(:))]; end
    
    hi = imagesc(B, ylim); hold on
    
        % put boundary lines on plot
        [d.u1, Bo] = unique(L);
        Bo = Bo + 0.5;
        y2 = size(L,2);
    
    % if more than one cells per class label
    if size(L,2) > size(XLabels,2)
        % line([x1 x2],[y1 y2])
        for bb = 1:size(Bo,2)-1
            line([Bo(bb) Bo(bb)], [0 y2+1], 'Color', 'k', 'LineWidth', 1);
            line([0 y2+1], [Bo(bb) Bo(bb)], 'Color', 'k', 'LineWidth', 1);
        end
        Bo_margin = ceil(Bo(2)/2);
        XTick = Bo + Bo_margin - 1;
    else
        XTick = 1:size(L,2);
    end

    set(gca, 'XTick', XTick, 'YTick', XTick, ...
        'FontSize', 8, ...
        'XAxisLocation', 'bottom');
        
    if iscell(XLabels) && isnumeric(L) % for LabelsPred
        set(gca, 'XTickLabel', XLabels(4:6), 'YTickLabel', XLabels(1:3));
    else
        set(gca, 'XTickLabel', XLabels, 'YTickLabel', XLabels);
    end
end