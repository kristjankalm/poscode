# poscode

This is the MATLAB code required to run simulations as described in 
"Reading positional codes with fMRI: Problems and solutions	"
10.1371/journal.pone.0176585

poscode.m is a self-containing MATLAB function that has been tested on MATLAB 2015a.

The 'Model' parameter (line 13) specifies which scenario to run. The models differ in terms of whether the data gets z-scored in the process and whether the voxels have "positional sensitivity". The positional sensitivity is actually always explicitly modelled, however, for the cases which it's not needed, the variance of the positional likelihood function is set an absurdly great number (e.g. 10^10) so that in practice the voxels respond uniformly to all positions.

For LDA it currently uses the MATLAB's own 'fitdiscr' function which is absent from MATLAB 2013 and below. This also means that the LDA function doesn't run on Octave. You can replace the LDA function with any of the freely available ones on the web (https://www.google.co.uk/search?q=linear+discriminant+analysis+matlab+code).

Similarly, z-scoring is done with MATLAB's 'zscore' function from the Statistics Toolbox, which might not be available for all MATLAB versions.
