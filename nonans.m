function [N]=nonans(M)
% NONANS  returns non-NaN items from matrix/vector 
% NONANS (M)
%     Example: 
%           A = [0 1 NaN 1 NaN];
%           A = nonans(A)
%   A =
%     1     1
 ...
if nargin < 1, disp('No input specified. '); return ; end


switch nargin
    case 1
        N = M(~isnan(M));
    case 2
        M(isnan(M)) = 0;
        N = M;
    otherwise       
        error('Invalid number of arguments, max 2');
end



        
